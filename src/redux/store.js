import { createStore, applyMiddleware, compose } from "redux";
import reducers from "../redux/reducers/_combine";
import middleware from "../redux/middleware";

const composeEnhancers =
  typeof window != "undefined"
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    : compose;
const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(...middleware))
);

export default store;
