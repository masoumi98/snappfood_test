import { LIST } from "../types";

import Req from "axios";

export function getFoodList(queryParam) {
  return async function (dispatch) {
    dispatch({
      type: LIST.RESTARUNTLIST_LOAD,
    });
    try {
      const headers = {
        "content-type": "application/json",
      };
      const res = await Req.get(
        `https://snappfood.ir/mobile/v3/restaurant/vendors-list?${queryParam}`,
        { headers: headers }
      );
      dispatch({
        type: LIST.RESTARUNTLIST_SUCCESS,
        payload: res?.data?.data,
      });
    } catch (err) {
      dispatch({
        type: LIST.RESTARUNTLIST_ERRORS,
        payload: err?.response?.data,
      });

      throw err;
    }
  };
}
