import { LIST } from "../types";

const initialState = {
  restaurntList: {
    content: {},
    status: "",
    error: {},
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LIST.RESTARUNTLIST_LOAD:
      return {
        status: "fetching",
      };
    case LIST.RESTARUNTLIST_SUCCESS:
      return {
        status: "success",
        content: action.payload,
      };
    case LIST.RESTARUNTLIST_ERRORS:
      return {
        status: "errors",
        error: action.payload,
      };
    default:
      return state;
  }
};
