import { combineReducers } from "redux";
import listReducer from "./list-reducer";

const reducers = combineReducers({
  list: listReducer,
});

export default reducers;
