import thunk from "../redux/middleware/thunk";

const middleware = [thunk];
export default middleware;
