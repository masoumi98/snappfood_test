import React from "react";

export default ({ item }) => (
  <div className="fooodItemBox">
    {item.data?.backgroundImage ? (
      <div
        className="imgSection"
        style={{
          background: `#eee url(${item.data?.backgroundImage}) center center no-repeat`,
          backgroundSize: "cover",
        }}
      ></div>
    ) : (
      <div className="imgSection"> بدون عکس</div>
    )}

    <div className="titleSection">
      <h3>{item.data?.title}</h3>
      <p>{item.data?.address}</p>
    </div>
  </div>
);
