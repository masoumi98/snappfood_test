import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getFoodList } from "../../redux/actions/list-actions";
import RestaurantItem from "./RestaurantItem";

function Index({ returant, getFoodList }) {
  const [fetching, setFetching] = useState(false);
  const [restaurantsList, setRestaurantsList] = useState(
    returant?.content?.finalResult || []
  );
  const [pageNumber, setPageNumber] = useState(0);
  const [filters, setFilters] = useState("");

  useEffect(() => {
    if (fetching && returant.status === "success") {
      setFetching(false);
      const { content } = returant;
      setRestaurantsList(
        filters !== "" && pageNumber === 0
          ? content?.finalResult
          : [...restaurantsList, ...content?.finalResult]
      );
    }
  }, [
    fetching,
    filters,
    returant,
    returant.status,
    restaurantsList,
    pageNumber,
  ]);

  useEffect(() => {
    // Fetching food list for first time
    getFoodList(`page=0&page_size=10`);
    setFetching(true);
    // eslint-disable-next-line
  }, []);

  const handleScroll = async (e) => {
    // Checking scroll is down
    if (e.target.scrollTop + e.target.clientHeight === e.target.scrollHeight) {
      setPageNumber(pageNumber + 1);
      const queryParam = `page=${pageNumber + 1}&page_size=10${
        filters && `&filters=${filters}`
      }`;
      // API call for get new Resturants List
      await getFoodList(queryParam);
      setFetching(true);
    }
  };

  const handleFilter = async (filterName) => {
    setPageNumber(0);
    setFilters(filterName);
    const queryParam = `page=${0}&page_size=10&filters=${filterName}`;
    await getFoodList(queryParam);
    setFetching(true);
  };

  return (
    <div className="app" onScroll={handleScroll}>
      <div className="container">
        <nav className="nav">
          <div onClick={() => handleFilter("nearest")}>نزدیک ترین</div>
        </nav>
        <article>
          {restaurantsList ? (
            restaurantsList?.map((item) => {
              return (
                <RestaurantItem
                  key={"Food_Item" + Math.random(99999999)}
                  item={item}
                />
              );
            })
          ) : (
            <p className="notFoundSection">موردی یافت نشد</p>
          )}
        </article>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  returant: state.list,
});

export default connect(mapStateToProps, { getFoodList })(Index);
