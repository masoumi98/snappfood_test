import React, { useState, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import routeOptions from "../client/routes";
import "../assert/main.scss";

function App() {
  const [routes, setRoutes] = useState([]);

  useEffect(() => {
    const routes = routeOptions.routes.map(
      ({ path, component, exact, layout, properties }) => (
        <Route
          key={"ROUTE_" + Math.random()}
          exact={exact}
          path={path}
          properties={properties}
          component={component}
        />
      )
    );
    setRoutes(routes);
    // eslint-disable-next-line
  }, []);

  return <Switch>{routes}</Switch>;
}

export default App;
