import React from "react";
import loadable from "@loadable/component";

const FoodList = loadable(() => import("../client/food-list/index"), {
  fallback: "",
});

export default {
  routes: [
    {
      path: "/",
      component: FoodList,
      exact: true,
    },
  ],
};
