import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "../src/redux/store";
import App from "../src/client/App";



ReactDOM.render(

      <BrowserRouter basename="/">
        <Provider store={store}>
          <Router basename={process.env.REACT_APP_ROUTER_BASE || '/'}>
              <App />
          </Router>
        </Provider>
      </BrowserRouter>,
  document.getElementById("root")
);

